<?php

    interface Crud_interface{
        
        public function create($entity);
        
        public function retrieve($entity);

        public function update($entity);

        public function delete($entity);
    }