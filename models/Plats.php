<?php
/**
 * Entité Carte
 */

    class Plats {
        private $id;
        private $id_menu;
        private $id_type;
        private $plat_name;
        private $prix;
        private $url_image;

        public function __construct($nom,$prix,$url=null,$type=null,$menu=null){
            $this->id_menu = $menu;
            $this->id_type = $type;
            $this->prix = $prix;
            $this->url = $url;
            $this->nom = $nom;
        }

        public function to_array(){
            $array = array(
                "id_menu"=> $this->id_menu,
                "id_type"=> $this->id_type,
                "nom"=> $this->nom,
                "prix"=> $this->dprix,
                "url"=> $this->url,
            );
            return $array;
        }

        public function to_json(){
            return json_encode($this->to_array());
        }
}