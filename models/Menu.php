<?php
/**
 * Entité user
 */

    class User {
        private $id;
        private $username;
        private $password;
        private $email;

        public function __construct($id=null,$username=null,$password=null,$email=null){
            $this->id =$id;
            $this->email =$email;
            $this->password =$password;
            $this->username =$username;
            if($id !== null){
                if($username === null && $password === null && $email === null){
                    $this->get($id);
                }
            }
        }
        
        public function to_array(){
            $array = array(
                "id_menu"=> $this->id_menu,
                "id_type"=> $this->id_type,
                "nom"=> $this->nom,
                "prix"=> $this->dprix,
                "url"=> $this->url,
            );
            return $array;
        }

        public function to_json(){
            return json_encode($this->to_array());
        }

    }