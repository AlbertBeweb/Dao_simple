<?php
/**
 * Entité user
 */

    class User {
        private $id;
        private $username;
        private $password;
        private $email;

        public function __construct($id=null,$username=null,$password=null,$email=null){
            $this->id =$id;
            $this->email =$email;
            $this->password =$password;
            $this->username =$username;
            if($id !== null){
                if($username === null && $password === null && $email === null){
                    $this->get($id);
                }
            }
        }
        
        public function to_array(){
            $array = array(
                "id"=>       $this->id,
                "email"=>    $this->email,
                "password"=> $this->password,
                "username"=> $this->username
            );
            return $array;
        }

        public function to_json(){
            
            return json_encode($this->to_array());
        }
};