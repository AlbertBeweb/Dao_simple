<?php
/**
 * Entité Carte
 */

    class User {
        private $id;
        private $id_restau;
        private $card_name;
        private $description;

        public function __construct($id_restau=null,$card_name=null,$description=null){
            $this->id_restau = $id_restau;
            $this->card_name = $card_name;
            $this->description = $description;
        }

        public function to_array(){
            $array = array(
                "id_restau"=>   $this->id_restau,
                "card_name"=>   $this->card_name,
                "description"=> $this->description,
            );
            return $array;
        }

        public function to_json(){
            return json_encode($this->to_array());
        }
}